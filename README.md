![preview](https://i.imgur.com/tUydzFd.jpg)

Requires Node v8.4 at least. untested in earlier versions.

This is a simple juice site i made in React using some nice animated css effects.

It also utlises lazy loading to only show images that appear on screen.

How to use.

1. Clone the repo.
2. run yarn inside the project directory or npm install
3. run yarn start or npm run start
4. that's it!

See the demo [here](https://crumble.gitlab.io/react-simple-juice-site/)
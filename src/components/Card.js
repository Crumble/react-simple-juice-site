import React, {Component} from 'react';
import './Card.css';
import LazyLoad from 'react-lazyload';
import { forceCheck } from 'react-lazyload';

export default class Card extends Component{

    componentDidMount(){
        setTimeout(forceCheck, 1000);
    }

    render(){
        return (

            <LazyLoad height={600} throttle={200}>

                <div className={this.props.card.animation}>

                    <div className={'front'} onClick={() => this.props.showBack(this.props.card)}>

                        <img src={'juice.jpg'} alt={'Vitamin Juice'} className={'card-image'}/>

                        <div className={'container'}>

                            <h3>Vitamin Juice <span className={'price'}>$24.99</span></h3>

                            <p>Need a jump on your vitamins while drinking?
                                Tired of popping pills? Drink our vitamin enhanced juice, available in several flavours.</p>

                        </div>

                    </div>

                    <div className={'container-back back'} onClick={() => this.props.showFront(this.props.card)}>

                        <h3>Vitamin Juice <span className={'price'}>$24.99</span></h3>

                        <p>{this.props.card.description}</p>

                    </div>

                </div>

            </LazyLoad>

        )
    }
}

